<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();

        $this->load->model('Menu_model', 'menu');
    }
    public function index()
    {
        $data['title'] = 'Menu Managament';
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('menu/index', $data);
        $this->load->view('template/footer');
    }

    function addMenu()
    {
        $id = $this->input->post('id');
        $data = [
            'menu' => $this->input->post('menu'),
            'order_number' => $this->input->post('order_number')
        ];
        if ($id) {
            $this->db->update('user_menu', $data, ['id' => $id]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu has been updated!</div>');
        } else {
            $this->db->insert('user_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu has been added!</div>');
        }
        redirect('menu');
    }

    function editMenu()
    {
        $id = $this->input->post('id');

        $data = $this->menu->getMenu($id)->row();

        echo json_encode($data);
    }

    function deleteMenu($id)
    {
        if ($this->db->delete('user_menu', ['id' => $id])) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu has been deleted!</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu failed deleted!</div>');
        }
        redirect('menu');
    }

    public function submenu()
    {
        $data['title'] = 'Submenu Managament';
        $data['menu'] = $this->menu->getMenu()->result_array();
        $data['submenu'] = $this->menu->getSubMenu();

        // template
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('menu/submenu', $data);
        $this->load->view('template/footer');
    }

    function addSubmenu()
    {
        $id = $this->input->post('id');
        $data = [
            'title' => $this->input->post('title'),
            'menu_id' => $this->input->post('menu_id'),
            'url' => $this->input->post('url'),
            'icon' => $this->input->post('icon'),
            'is_active' => $this->input->post('is_active')
        ];

        if ($id) {
            $this->db->update('user_sub_menu', $data, ['id' => $id]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Sub menu has been updated!</div>');
        } else {
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Sub menu has been added!</div>');
        }
        redirect('menu/submenu');
    }

    function editSubmenu()
    {
        $id = $this->input->post('id');

        $data = $this->menu->get_detail_submenu($id);
        echo json_encode($data);
    }

    function deleteSubmenu($id)
    {
        if ($this->db->delete('user_sub_menu', ['id' => $id])) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Sub menu has been deleted!</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Sub menu failed deleted!</div>');
        }
        redirect('menu/submenu');
    }
}
