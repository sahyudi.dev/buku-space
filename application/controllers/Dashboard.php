<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('book_');
    }

    public function index()
    {
        $data['title'] = 'Home';
        $data['bookTotal'] = $this->book_->get_total_book()->row()->total;
        $data['borrow_book'] = $this->book_->getBorrowBooks(1)->num_rows();
        $data['ready_book'] = $this->book_->get_total_book_ready()->row()->total;
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('dashboard/index', $data);
        $this->load->view('template/footer');
    }
}
