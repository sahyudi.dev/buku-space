<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }
    public function index()
    {
        $data['title'] = 'All Member';

        $data['member'] = $this->db->get_where('user', ['role_id' => 2])->result();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('member/index', $data);
        $this->load->view('template/footer');
    }

    function edit()
    {
        $id = $this->input->post('id');
        $data = $this->db->get_where('user', ['id' => $id])->row();
        echo json_encode($data);
    }

    function update()
    {
        $id = $this->input->post('id');
        $data = [
            'name' => $this->input->post('name'),
            'is_active' => $this->input->post('is_active'),
        ];
        if ($this->db->update('user', $data, ['id' => $id])) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Member has been success updated!</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Member failed updated!</div>');
        }
        redirect('member');
    }

    function delete($id)
    {

        if ($this->db->delete('user', ['id' => $id])) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Member has been success deleted!</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Member failed deleted!</div>');
        }
        redirect('member');
    }

}
