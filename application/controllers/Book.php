<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Book extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();

        $this->load->model('book_');
    }
    public function index()
    {
        $data['title'] = 'All Book';
        $data['book'] = $this->book_->get_book()->result();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('book/index', $data);
        $this->load->view('template/footer');
    }

    public function add()
    {
        $id = $this->input->post('id');
        $data = [
            'date_created' => time(),
            'date_buy' => $this->input->post('date_buy'),
            'author' => $this->input->post('author'),
            'title' => $this->input->post('title'),
            'publisher' => $this->input->post('publisher'),
            'years' => $this->input->post('years'),
            'isbn' => $this->input->post('isbn'),
            'qty' => $this->input->post('qty'),
            'ready_status' => $this->input->post('qty'),
            'descrip' => $this->input->post('descrip'),
            'user_id' => $this->session->userdata('id')
        ];

        if ($id) {
            $this->db->update('book_user', $data, ['id' => $id]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Book has been success updated</div>');
        } else {
            $this->db->insert('book_user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New book has been success added</div>');
        }
        redirect('book');
    }

    public function edit()
    {
        $id = $this->input->post('id');
        $data = $this->book_->get_book($id)->row();
        echo json_encode($data);
    }

    public function delete($id)
    {
        if ($this->db->delete('book_user', ['id' => $id])) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Book has been deleted!</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Book failed delete!</div>');
        }

        echo '<script> window.history.go(-1) </script>';
    }

    function borrowBook()
    {
        $data['title'] = 'Borrowed Books';
        $data['book'] = $this->book_->getBorrowBooks()->result();
        $data['book_ready'] = $this->book_->getReadyBooks()->result();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('book/borrow-book', $data);
        $this->load->view('template/footer');
    }

    function addBorrow()
    {
        $book_id = $this->input->post('book');
        $data = [
            'book_id' => $book_id,
            'name' => $this->input->post('name'),
            'no_hp' => $this->input->post('no_hp'),
            'status' => 1,
            'borrow_date' => $this->input->post('borrow_date'),
            'created_at' => date('Y-m-d H:i:s')
        ];

        if ($this->db->insert('book_borrowed', $data)) {
            $update_data = $this->db->query("UPDATE book_user SET ready_status = ready_status - 1 WHERE id = $book_id ");

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Book has been success borrowed</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Book has been failed borrowed</div>');
        }

        echo '<script> window.history.go(-1) </script>';
    }

    function bookReturn()
    {
        $data['title'] = 'Returned Books';
        $data['book'] = $this->book_->getReturnBooks()->result();
        $data['book_borrows'] = $this->book_->getBorrowBooks(1)->result();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('book/return-book', $data);
        $this->load->view('template/footer');
    }

    function addReturn()
    {
        $borrow_id = $this->input->post('borrow_id');
        $data_borrow = $this->db->get_where('book_borrowed', ['id' => $borrow_id])->row();
        // var_dump($data_borrow);
        // die;
        $data = [
            'borrow_id' => $borrow_id,
            'return_date' => $this->input->post('return_date'),
            'remark' => $this->input->post('remark'),
            'created_at' => date('Y-m-d H:i:s')
        ];

        if ($this->db->insert('book_returned', $data)) {
            $this->db->query("UPDATE book_user SET ready_status = ready_status + 1 WHERE id = $data_borrow->book_id ");
            $this->db->query("UPDATE book_borrowed SET `status` = `status` - 1 WHERE id = $borrow_id ");

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Book has been success returned</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Book has been failed returned</div>');
        }

        echo '<script> window.history.go(-1) </script>';
    }
}
