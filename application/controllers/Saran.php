<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Saran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }


    function index()
    {
        $data['title'] = 'Saran Box';
        $this->load->view('auth/header', $data);
        $this->load->view('saran/form');
        $this->load->view('auth/footer');
    }

    function add()
    {
        $data = [
            'nama' => $this->input->post('name'),
            'saran' => $this->input->post('saran'),
        ];

        $this->db->insert('box_saran', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Terimkasih atas saran yang diberikan, bantu kami untuk menjadi lebih baik !</div>');
        redirect('saran');
    }

    function get_saran()
    {
        $data['title'] = 'Box Saran';
        $data['saran'] = $this->db->get('box_saran')->result();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view('saran/index', $data);
        $this->load->view('template/footer');
    }
}
