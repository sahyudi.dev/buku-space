<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800"><?= $title ?></h1>
    <div class="row">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>

            <a href="#" class="btn btn-primary btn-md float-right mb-3" data-toggle="modal" data-target="#form-book">Add Return</a>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Borrower</th>
                            <th>Title</th>
                            <th>Return Date</th>
                            <!-- <th>Actions</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($book as $key => $value) { ?>
                            <tr>
                                <td class="text-center"><?= $key + 1 ?></td>
                                <td><?= $value->name ?></td>
                                <td><?= $value->title ?></td>
                                <td><?= date('d F Y', strtotime($value->return_date)) ?></td>
                                <!-- <td>
                                    <a href="#" data-id="<?= $value->id; ?>" data-target="#form-book" data-toggle="modal" class="badge badge-success btn-edit">Edit</a>
                                    <a href="#" onclick="confirm_delete(<?= $value->id ?>)" class="badge badge-danger">delete</a>
                                </td> -->
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="form-book" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Return</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('book/addReturn') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Date Return</label>
                        <input type="date" class="form-control" id="return_date" name="return_date" value="<?= date('Y-m-d') ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Borrower - Title</label>
                        <select class="form-control" name="borrow_id" id="borrow_id" placeholder="Select One" required>
                            <option></option>
                            <?php foreach ($book_borrows as $key => $value) { ?>
                                <option value="<?= $value->id ?>"><?= $value->name . ' - ' . $value->title ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Name</label>
                        <textarea name="remark" id="remark" class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Return</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `<?= base_url('book/delete/') ?>${id}`;
            }
        })
    }
    $('.btn-edit').on('click', function() {
        const id = $(this).data('id');
        $.ajax({
            url: "<?= base_url('book/edit') ?>",
            data: {
                id: id
            },
            type: "post",
            dataType: 'JSON',
            success: function(data) {
                $('#id').val(data.id);
                $('#date_buy').val(data.date_buy);
                $('#publisher').val(data.publisher);
                $('#author').val(data.author);
                $('#title').val(data.title);
                $('#years').val(data.years);
                $('#isbn').val(data.isbn);
                $('#qty').val(data.qty);
                $('#descrip').val(data.descrip);
            }
        })
    });
</script>