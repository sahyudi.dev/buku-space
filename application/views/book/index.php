<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800"><?= $title ?></h1>
    <div class="row">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>

            <a href="#" class="btn btn-primary btn-md float-right mb-3" data-toggle="modal" data-target="#form-book">Add New Book</a>
            <div class="table-responsive">
                <table class="table table-stripped table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Author</th>
                            <th>Title</th>
                            <th>Publisher - Year</th>
                            <th>Quantity</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($book as $key => $value) { ?>
                            <tr>
                                <td class="text-center"><?= $key + 1 ?></td>
                                <td><?= $value->author ?></td>
                                <td><?= $value->title ?></td>
                                <td><?= $value->publisher . ' - ' . $value->years ?></td>
                                <td class="text-center"><?= $value->qty ?></td>
                                <td>
                                    <?php if ($value->ready_status > 0) {
                                        echo "<span class='badge badge-info'>Ready</span>";
                                    } else {
                                        echo "<span class='badge badge-warning'>Borrow</span>";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="#" data-id="<?= $value->id; ?>" data-target="#form-book" data-toggle="modal" class="badge badge-success btn-edit">Edit</a>
                                    <a href="#" onclick="confirm_delete(<?= $value->id ?>)" class="badge badge-danger">delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="form-book" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('book/add') ?>" method="post">
                <input type="text" name="id" id="id" hidden>
                <div class="modal-body row">
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">Tanggal Beli</label>
                        <input type="date" class="form-control" id="date_buy" name="date_buy">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">Penulis</label>
                        <input type="text" class="form-control" name="author" id="author">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">Judul</label>
                        <input type="text" class="form-control" name="title" id="title">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">Publisher</label>
                        <input type="text" class="form-control" name="publisher" id="publisher">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">Tahun</label>
                        <input type="text" class="form-control" name="years" id="years">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">ISBN</label>
                        <input type="text" class="form-control" name="isbn" id="isbn">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">Jumlah</label>
                        <input type="text" class="form-control" name="qty" id="qty">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="recipient-name" class="col-form-label">Deskcripsi</label>
                        <textarea name="descrip" id="descrip" class="form-control"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `<?= base_url('book/delete/') ?>${id}`;
            }
        })
    }
    $('.btn-edit').on('click', function() {
        const id = $(this).data('id');
        $.ajax({
            url: "<?= base_url('book/edit') ?>",
            data: {
                id: id
            },
            type: "post",
            dataType: 'JSON',
            success: function(data) {
                $('#id').val(data.id);
                $('#date_buy').val(data.date_buy);
                $('#publisher').val(data.publisher);
                $('#author').val(data.author);
                $('#title').val(data.title);
                $('#years').val(data.years);
                $('#isbn').val(data.isbn);
                $('#qty').val(data.qty);
                $('#descrip').val(data.descrip);
            }
        })
    });
</script>