<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title ?></h1>
    <div class="row">
        <div class="col-lg-12">
            <?= $this->session->flashdata('message'); ?>
            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Email</th>
                            <th scope="col">Is Active</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($member as $key => $value) { ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $value->name ?></td>
                                <td><?= $value->email ?></td>
                                <td>
                                    <?php
                                    if ($value->is_active == 1) {
                                        echo "<span class='badge badge-success'>Verified</span>";
                                    } else {
                                        echo "<span class='badge badge-danger'>Not Verified</span>";
                                    }
                                    ?>
                                </td>
                                <td><?= date('d F Y', strtotime($value->date_created)) ?></td>
                                <td>
                                    <a href="#" onclick="edit_member(<?= $value->id ?>)" data-toggle="modal" data-target="#modal-member" class="badge badge-success">Edit</a>
                                    <a href="#" onclick="confirm_delete(<?= $value->id ?>)" class="badge badge-danger">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-member" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('member/update') ?>" method="post">
                <input type="text" name="id" id="id" hidden>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Email</label>
                        <input type="text" class="form-control" id="email" name="email" disabled>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Name</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Is Active</label>
                        <select name="is_active" id="is_active" class="form-control">
                            <option value="0">Not Verified</option>
                            <option value="1">Active</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function edit_member(id) {
        $.ajax({
            url: "<?= base_url('member/edit') ?>",
            data: {
                id: id
            },
            type: "post",
            dataType: 'JSON',
            success: function(data) {
                $('#id').val(data.id);
                $('#email').val(data.email);
                $('#name').val(data.name);
                $('#is_active').val(data.is_active);
            }
        })
    }

    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `<?= base_url('member/delete/') ?>${id}`;
            }
        })
    }
</script>