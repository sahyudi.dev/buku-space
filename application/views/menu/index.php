<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title ?></h1>

    <div class="row">
        <div class="col-lg-6">
            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>') ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#modal-menu">Add New Menu</a>
            <table class="table table-hover ">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Menu</th>
                        <th scope="col">Order Number</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($menu as $m) { ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $m['menu'] ?></td>
                            <td><?= $m['order_number'] ?></td>
                            <td>
                                <a href="#" onclick="edit_menu(<?= $m['id'] ?>)" data-toggle="modal" data-target="#modal-menu" class="badge badge-success">Edit</a>
                                <a href="#" onclick="confirm_delete(<?= $m['id'] ?>)" class="badge badge-danger">delete</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal -->
<div class="modal fade" id="modal-menu" tabindex="-1" role="dialog" aria-labelledby="modal-menu" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="neMenuModalLabel">Add New Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('menu/addMenu') ?>" method="post">
                <input type="text" id="id" name="id" hidden>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="menu" name="menu" placeholder="Menu name">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="order_number" name="order_number" placeholder="Order number">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function edit_menu(id) {
        $.ajax({
            url: "<?= base_url('menu/editMenu') ?>",
            data: {
                id: id
            },
            type: "post",
            dataType: 'JSON',
            success: function(data) {
                $('#id').val(data.id);
                $('#menu').val(data.menu);
                $('#order_number').val(data.order_number);
            }
        })
    }

    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `<?= base_url('menu/deleteMenu/') ?>${id}`;
            }
        })
    }
</script>