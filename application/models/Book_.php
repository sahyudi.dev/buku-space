<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Book_ extends CI_Model
{

    function get_book($id = null)
    {
        $this->db->Select('*');
        $this->db->from('book_user');
        if ($id) {
            $this->db->where('id', $id);
        }
        $this->db->where('user_id', $this->session->userdata('id'));
        return $this->db->get();
    }

    function getBorrowBooks($status = null)
    {
        $this->db->Select('A.*, B.title');
        $this->db->from('book_borrowed A');
        $this->db->join('book_user B', 'A.book_id = B.id');
        $this->db->where('B.user_id', $this->session->userdata('id'));
        if ($status) {
            $this->db->where('A.status', $status);
        }
        return $this->db->get();
    }

    function getReadyBooks()
    {
        $this->db->Select('*');
        $this->db->from('book_user');
        $this->db->where('user_id', $this->session->userdata('id'));
        $this->db->where('ready_status >', 0);
        return $this->db->get();
    }

    function getReturnBooks()
    {
        $this->db->Select('A.*, B.name, C.title');
        $this->db->from('book_returned A');
        $this->db->join('book_borrowed B', 'A.borrow_id = B.id');
        $this->db->join('book_user C', 'B.book_id = C.id');
        $this->db->where('C.user_id', $this->session->userdata('id'));
        return $this->db->get();
    }

    function get_total_book()
    {
        $this->db->select('SUM(qty) as total');
        $this->db->where('user_id', $this->session->userdata('id'));
        return $this->db->get('book_user');
    }
    function get_total_book_ready()
    {
        $this->db->Select('sum(ready_status) as total');
        $this->db->from('book_user');
        $this->db->where('user_id', $this->session->userdata('id'));
        // $this->db->where('ready_status >', 0);
        return $this->db->get();
    }
}
