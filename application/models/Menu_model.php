<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function getSubMenu()
    {
        $this->db->select('SM.*, M.menu');
        $this->db->join('user_menu M', 'SM.menu_id = M.id');
        return $this->db->get('user_sub_menu SM')->result_array();
    }

    function get_detail_submenu($id)
    {
        return $this->db->get_where('user_sub_menu', ['id' => $id])->row();
    }

    function getMenu($id = null)
    {
        $this->db->select('*');
        if ($id) {
            $this->db->where('id', $id);
        }
        return $this->db->get('user_menu');
    }
}
