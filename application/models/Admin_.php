<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_ extends CI_Model
{

    function get_books($id = null)
    {
        $this->db->Select('A.*, B.name as member');
        $this->db->from('book_user A');
        $this->db->join('user B', 'A.user_id = B.id');
        if ($id) {
            $this->db->where('id', $id);
        }
        return $this->db->get();
    }
}
